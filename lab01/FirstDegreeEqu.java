import javax.swing.JOptionPane;
public class FirstDegreeEqu {
    public static void main(String[] args) {
        String strNum1, strNum2;
        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        String strNotification = "The result of equation " + strNum1 +"x + " + strNum2 + "= 0 is x = ";
        double a = Double.parseDouble(strNum1);
        double b = Double.parseDouble(strNum2);
        if(a != 0){
            double result = -b / a;
            JOptionPane.showMessageDialog(null, strNotification + result);
        }
        else{
            JOptionPane.showMessageDialog(null, strNotification + "no solution");
        }
        System.exit(0);
    }
}
