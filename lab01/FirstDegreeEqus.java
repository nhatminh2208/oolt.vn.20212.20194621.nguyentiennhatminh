import javax.swing.JOptionPane;
public class FirstDegreeEqus {
    public static void main(String[] args) {
        String strNum1, strNum2, strNum3, strNum4, strNum5, strNum6;
        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        strNum3 = JOptionPane.showInputDialog(null, "Please input the third number: ", "Input the third number", JOptionPane.INFORMATION_MESSAGE);
        strNum4 = JOptionPane.showInputDialog(null, "Please input the fouth number: ", "Input the fouth number", JOptionPane.INFORMATION_MESSAGE);
        strNum5 = JOptionPane.showInputDialog(null, "Please input the fifth number: ", "Input the fifth number", JOptionPane.INFORMATION_MESSAGE);
        strNum6 = JOptionPane.showInputDialog(null, "Please input the sixth number: ", "Input the sixth number", JOptionPane.INFORMATION_MESSAGE);
        String strNotification = "The result of equations " + strNum1 +"x1 + " + strNum2 + "x2 = " + strNum5 +  " and " + strNum3 +"x1 + " + strNum4 + "x2 = " + strNum6 + " has: ";
        double a11 = Double.parseDouble(strNum1);
        double a12 = Double.parseDouble(strNum2);
        double a21 = Double.parseDouble(strNum3);
        double a22 = Double.parseDouble(strNum4);
        double b1 = Double.parseDouble(strNum5);
        double b2 = Double.parseDouble(strNum6);
        double D = a11 * a22 - a21 * a12;
        double D1 = b1 * a22 - b2 * a12;
        double D2 = a11 * b2 - a21* b1;
        if(D != 0){
            double result1 = D1 / D;
            double result2 = D2 / D;
            JOptionPane.showMessageDialog(null, strNotification + "unique solution (x1, x2) = (" + result1 + "," + result2 + ")");
        }
        else if(D == 0 && D1 == 0 && D2 == 0){
            JOptionPane.showMessageDialog(null, strNotification + " infinitely solution");
        }
        else{
            JOptionPane.showMessageDialog(null, strNotification + " no solution");
        }
        System.exit(0);
    }
}
